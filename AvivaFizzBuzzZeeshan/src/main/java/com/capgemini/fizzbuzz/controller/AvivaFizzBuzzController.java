package com.capgemini.fizzbuzz.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.experimental.theories.suppliers.TestedOn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.fizzbuzz.service.IManageAvivaFizzBuzzService;
@RestController
@RequestMapping(value = "/FizzBuzz")
public class AvivaFizzBuzzController {
	@Autowired
	private IManageAvivaFizzBuzzService fizzbuzzService;

	@RequestMapping(value = "/positiveinteger/{number}", method = RequestMethod.GET, produces = "application/json")
	public List<String> ShowFizzBuzzByDivisibilty(@PathVariable("number")String num) {
		
		return fizzbuzzService.ShowFizzBuzzByDivisibilty(num);

	}
	
	@RequestMapping(value="/pagination")
	public String substituteFizzAndBuzz(@RequestParam("pageNo")String pageNo,@RequestParam("action")String action){
		return"";
	}

}
