package com.capgemini.fizzbuzz.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
@Service
public class ManageAvivaFizzBuzzServiceImpl implements IManageAvivaFizzBuzzService {

	public List<String> ShowFizzBuzzByDivisibilty(String num) {
		List<String> fizzBuzzStrings=new ArrayList<>();
		int number = Integer.parseInt(num);
		for(int i=1;i<=number;i++){
		if (i % 3 == 0) {
			if (i % 5 == 0) {
				 fizzBuzzStrings.add("fizzbuzz");
				continue;
			} else {
				 fizzBuzzStrings.add("fizz");
				continue;
			}
		} else if (i % 5 == 0) {
			 fizzBuzzStrings.add("buzz");
			continue;
		}
		 fizzBuzzStrings.add(String.valueOf(i));
	}
		SimpleDateFormat format=new SimpleDateFormat("EEEE");
		String weekday=format.format(new Date());
		System.out.println(weekday);
		if("Saturday".equalsIgnoreCase(weekday)){
			for(int i=0;i<fizzBuzzStrings.size();i++){
				if(fizzBuzzStrings.get(i).contains("fizz")){
					fizzBuzzStrings.get(i).replace("fizz", "wizz");
				}
				if(fizzBuzzStrings.get(i).contains("buzz")){
					fizzBuzzStrings.get(i).replace("buzz", "wuzz");
				}

			}
		}
		return fizzBuzzStrings;
	}
}
