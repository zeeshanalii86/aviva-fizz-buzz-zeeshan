package com.capgemini.fizzbuzz.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages={"com.capgemini.fizzbuzz.service"})
public class RootConfig {

}
