package com.capgemini.fizzbuzz.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
@Configuration
@EnableWebMvc
@ComponentScan(basePackages={"com.capgemini.fizzbuzz.controller"})
public class MvcConfig extends WebMvcConfigurerAdapter {
 
}
